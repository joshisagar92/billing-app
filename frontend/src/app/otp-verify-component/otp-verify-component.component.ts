import {AfterContentInit, Component, OnInit} from '@angular/core';
import { WindowService } from "../service/window.service";
import {AuthenticationService} from "../service/auth/authentication.service";
import firebase from "firebase";
import auth = firebase.auth;
import {Router} from "@angular/router";

@Component({
  selector: 'app-otp-verify-component',
  templateUrl: './otp-verify-component.component.html',
  styleUrls: ['./otp-verify-component.component.css']
})
export class OtpVerifyComponentComponent implements OnInit,AfterContentInit {

  firstOtpNo: string = "";
  secondOtpNo: string = "";
  thirdOtpNo: string = "";
  fourthOtpNo: string = "";
  fifthOtpNo: string = "";
  sixthOtpNo: string = "";
  timeLeft: number = 60;
  interval:any;
  showResend: boolean =  true;
  windowRef : any;
  lastThreeDigit: string = "";

  constructor(private windowService : WindowService,
              private authService:AuthenticationService,
              private router: Router) {
      this.windowRef = this.windowService.windowRef
  }

  ngAfterContentInit(): void {

  }

  ngOnInit(): void {

    this.lastThreeDigit = this.authService.mobileNumber.slice(7, 10);
    this.startTimer();
  }

  verifyOtp() {
    let otp = this.firstOtpNo + this.secondOtpNo +this.thirdOtpNo + this.fourthOtpNo + this.fifthOtpNo + this.sixthOtpNo;
    console.log(otp);
    this.windowRef.OtpConfirmation.confirm(otp).then((response: object) => {

      this.router.navigate(["/user-details"]);


    }).catch((reason: object) => {
      console.log(reason)
    })
  }

  resendCode() {

    this.windowRef.recaptchaVerifier = new auth.RecaptchaVerifier('resendButton', {
      size: 'invisible',
      callback: (response: object) => {
        //console.log("dsdsdsd");
      }
    });
    this.windowRef.recaptchaVerifier.render();

    this.showResend = true;
    this.startTimer();
    this.authService.sendOtp(this.authService.mobileNumber, this.windowRef.recaptchaVerifier)
      .then(confirmation => {
        this.windowRef.OtpConfirmation = confirmation;
      });
  }

  startTimer() {
    this.timeLeft = 60;
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.showResend = false;
        clearInterval(this.interval);
      }
    },1000)
  }

  GotoHomeScreen() {
    this.windowService.showOtpLogin = true;
    this.windowService.showVerifyOtp = false;
    this.windowService.showSingIn = false;
    this.windowService.showSingUp = false;
    this.windowService.showRecoverPassword = false;
  }

}
