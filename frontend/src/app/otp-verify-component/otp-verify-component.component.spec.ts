import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtpVerifyComponentComponent } from './otp-verify-component.component';

describe('OtpVerifyComponentComponent', () => {
  let component: OtpVerifyComponentComponent;
  let fixture: ComponentFixture<OtpVerifyComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtpVerifyComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtpVerifyComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
