import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AppFirebaseModule} from "./app-firebase/app-firebase.module";
import {FormsModule} from "@angular/forms";
import { OtpComponentComponent } from './otp-component/otp-component.component';
import {WindowService} from "./service/window.service";
import { OtpVerifyComponentComponent } from './otp-verify-component/otp-verify-component.component';
import {AuthenticationService} from "./service/auth/authentication.service";
import { SinginComponentComponent } from './singin-component/singin-component.component';
import {NgPasswordValidatorModule} from "ng-password-validator";
import { UserDetailsComponent } from './userdetails/user-details/user-details.component';
import {HttpClientModule} from "@angular/common/http";
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    OtpComponentComponent,
    OtpVerifyComponentComponent,
    SinginComponentComponent,
    UserDetailsComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppFirebaseModule,
    FormsModule,
    NgPasswordValidatorModule,
    HttpClientModule
  ],
  providers: [WindowService,AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
