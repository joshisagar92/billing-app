import {AfterContentInit, Component, OnInit} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {environment} from "../../environments/environment";
import firebase from "firebase";
import auth = firebase.auth;
import {WindowService} from "../service/window.service";
import {AuthenticationService} from "../service/auth/authentication.service";


@Component({
  selector: 'app-otp-component',
  templateUrl: './otp-component.component.html',
  styleUrls: ['./otp-component.component.css']
})
export class OtpComponentComponent implements OnInit, AfterContentInit {

  countries : any = [{"name":"United Kingdom","dial_code":"+44","code":"GB"},{"name":"United States","dial_code":"+1","code":"US"}
    ,{"name":"India","dial_code":"+91","code":"IN"}];

  countryDialCode : string = "+44";
  mobileNumber: string = "";
  windowRef : any;

  constructor(private authenticator : AngularFireAuth,
              private windowService : WindowService,
              private authService : AuthenticationService) {
  }
  ngOnInit(): void {
    this.windowRef = this.windowService.windowRef;
  }
  ngAfterContentInit(): void {

    //console.log("content Loaded"+ this.appName);
    try {
      firebase.initializeApp(environment.firebase);
    }catch (e) {
      console.log();
    }
    this.showCaptcha();
  }

  private showCaptcha() {
    this.windowRef.recaptchaVerifier = new auth.RecaptchaVerifier('sendOTPButton', {
      size: 'invisible',
      callback: (response: object) => {
        //console.log("dsdsdsd");
      }
    });

   // this.windowRef.recaptchaVerifier.render();
  }

  sendOTP() {
    let mobileno = this.countryDialCode + this.mobileNumber;
    this.authService.mobileNumber = mobileno;

    this.authService.sendOtp(mobileno,this.windowRef.recaptchaVerifier)
      .then(confirmation => {
        console.log("confirmation ------");
        console.log(confirmation);
        this.windowRef.OtpConfirmation = confirmation;
        this.windowService.showOtpLogin = false;
        this.windowService.showVerifyOtp = true;
        this.windowService.showSingIn = false;
        this.windowService.showSingUp = false;
        this.windowService.showRecoverPassword = false;
      })
  }

  singInWithGoogle() {
    this.authenticator.signInWithPopup(new auth.GoogleAuthProvider())
      .then(res =>{
        console.log(res);
      }).catch(reason => {
      console.log(reason);
    })
  }

  sinUpWithEmail() {
    this.windowService.showOtpLogin = false;
    this.windowService.showVerifyOtp = false;
    this.windowService.showSingIn = true;
    this.windowService.showSingUp = false;
    this.windowService.showRecoverPassword = false;
  }

  goToSingUpScreen() {
    this.windowService.showOtpLogin = false;
    this.windowService.showVerifyOtp = false;
    this.windowService.showSingIn = false;
    this.windowService.showSingUp = true;
    this.windowService.showRecoverPassword = false;
  }


}
