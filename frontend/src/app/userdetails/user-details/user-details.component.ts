import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  firstname: string = "";
  lastname: string = "";
  email: string = "";
  mobileno: string = "";
  location: string = "";
  showFirstStep :boolean = true;
  showSecondStep :boolean = false;
  showThirdStep :boolean = false;
  addressLine1: string = "";
  addressLine2: string = "";
  town: string = "";
  postcode: string = "";
  country: string = "";
  noOfCars: number = 0;
  vehicales: any[] = [];
  publicTransportTimes: number = 0;
  taxiTransportTimes: number = 0;
  airTransportTimes: number = 0;
  showFourthStep: boolean = false;
  electricitySupplier: string = "";
  gasSupplierSame: boolean = false;
  gasSupplier: string = "";
  buildingGeneratingElectricity: boolean = false;
  showFifthStep: boolean = false;
  eatingHabits: string = "";
  nonVegFrequency: number = 0;
  onlineShopFrequency: number = 0;
  petFrequency: number = 0;

  userDetails :any = {};

  registartionDetail : any = {
    registrationNumber: '',
  }





  constructor(private http: HttpClient, private router : Router) { }

  ngOnInit(): void {
  }

  saveUserDetailsStep1() {
    this.showFirstStep = false;
    this.showSecondStep = true;
    this.showThirdStep =false;
    this.showFourthStep = false;
    this.showFifthStep = false;

  }

  saveUserDetailsStep2() {
    this.showFirstStep = false;
    this.showSecondStep = false;
    this.showThirdStep = true;
    this.showFourthStep = false;
    this.showFifthStep = false;
  }

  goBackToStep1() {
    this.showFirstStep = true;
    this.showSecondStep = false;
    this.showThirdStep = false;
    this.showFourthStep = false
    this.showFifthStep = false;
  }

  addVehicleRegistrationField() {
    for (let i = 0; i < this.noOfCars ;i++){
      this.vehicales.push(this.registartionDetail)
    }
  }

  goBackToStep2() {
    this.showFirstStep = false;
    this.showSecondStep = true;
    this.showThirdStep = false;
    this.showFourthStep = false;
    this.showFifthStep = false;
  }

  saveUserDetailsStep3() {
    this.showFirstStep = false;
    this.showSecondStep = false;
    this.showThirdStep = false;
    this.showFourthStep = true;
    this.showFifthStep = false;
  }

  goBackToStep3() {
    this.showFirstStep = false;
    this.showSecondStep = false;
    this.showThirdStep = true;
    this.showFourthStep = false;
    this.showFifthStep = false;
  }

  saveUserDetailsStep5() {
    this.showFirstStep = false;
    this.showSecondStep = false;
    this.showThirdStep = false;
    this.showFourthStep = false;
    this.showFifthStep = true;
  }

  goBackToStep4() {
    this.showFirstStep = false;
    this.showSecondStep = false;
    this.showThirdStep = false;
    this.showFourthStep = true;
    this.showFifthStep = false;
  }

  saveUserDetails() {

    this.userDetails = {
      "firstname":this.firstname,
      "lastname":this.lastname,
      "email":this.email,
      "mobileno":this.mobileno,
      "location":this.location,
      "addressLine1":this.addressLine1,
      "addressLine2":this.addressLine2,
      "town":this.town,
      "postcode":this.postcode,
      "country":this.country,
      "noOfCars":this.noOfCars,
      "publicTransportTimes":this.publicTransportTimes,
      "taxiTransportTimes":this.taxiTransportTimes,
      "airTransportTimes":this.airTransportTimes,
      "electricitySupplier":this.electricitySupplier,
      "gasSupplier":this.gasSupplierSame? this.electricitySupplier:this.gasSupplier,
      "buildingGeneratingElectricity":this.buildingGeneratingElectricity,
      "eatingHabits":this.eatingHabits,
      "nonVegFrequency":this.nonVegFrequency,
      "onlineShopFrequency":this.onlineShopFrequency,
      "petFrequency":this.petFrequency,
      "registrationNumbers":[]
    };

    let strRegistrationNumbers = "";

    for (let i = 0; i < this.noOfCars ;i++){
      if(strRegistrationNumbers == ""){
        strRegistrationNumbers = this.vehicales[i].registrationNumber;
      }else{
        strRegistrationNumbers += ","+this.vehicales[i].registrationNumber;
      }
    }

    this.userDetails.registrationNumbers =strRegistrationNumbers;
    console.log(this.userDetails);
     this.http.post<any>('http://localhost:8080/users', this.userDetails).subscribe(data => {
       this.router.navigate(["/dashboard"]);
    })

  }
}
