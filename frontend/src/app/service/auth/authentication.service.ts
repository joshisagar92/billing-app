import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";

@Injectable()
export class AuthenticationService {

  mobileNumber:string = "";

  constructor(private authenticator : AngularFireAuth) { }

  sendOtp(mobileno: string, appVerifier: any) {
    return this.authenticator.signInWithPhoneNumber(mobileno,appVerifier);
  }
}
