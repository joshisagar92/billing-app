import { Injectable } from '@angular/core';

@Injectable()
export class WindowService {

  showOtpLogin:boolean = true;
  showVerifyOtp:boolean = false;
  showSingUp:boolean = false;
  showSingIn: boolean = false;
  showRecoverPassword: boolean = false;

  get windowRef() {
    return window;
  }

}
