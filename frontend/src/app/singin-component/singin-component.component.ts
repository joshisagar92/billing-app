import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {WindowService} from "../service/window.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-singin-component',
  templateUrl: './singin-component.component.html',
  styleUrls: ['./singin-component.component.css']
})
export class SinginComponentComponent implements OnInit {

  title = 'frontend';
  windowRef: any;
  singUpEmailAddress: string = "";
  singUpLoginPassword: string = "";
  isPasswordValid: boolean = false;

  options: object = {
    'password': {
      'type': "range",
      'min': 6,
      'max': 16,
    },
    'shadow': false,
    'offset': 15,
  };
  displayErrorMessage: boolean = false;
  displaySignInErrorMessage: boolean = false;
  error: string = "";
  signInError: string = "";
  singInEmail: string = "";
  signInPassword: string = "";
  recoverEmail: string = "";

  constructor(private authenticator : AngularFireAuth,
              public windowService : WindowService,
              private router: Router) {

  }

  ngOnInit(): void {
    this.windowRef = this.windowService.windowRef;
  }

  gotoHomeScreen() {
    this.windowService.showSingUp = false;
    this.windowService.showSingIn = false;
    this.windowService.showVerifyOtp = false;
    this.windowService.showOtpLogin = true;
    this.windowService.showRecoverPassword = false;
  }

  singUpWithEmailPassword() {
    this.authenticator.createUserWithEmailAndPassword(this.singUpEmailAddress,this.singUpLoginPassword)
      .then(value => {
        this.router.navigate(["/user-details"]);
      }).catch(reason => {
      this.displayErrorMessage = true;
      this.error = reason.message;

      setTimeout(()=>{
        this.displayErrorMessage = false;
        this.error = "";
      },5000);
    })
  }

  isValid(event: boolean) {
    this.isPasswordValid = event;
  }

  singInWithUnPw() {
    this.authenticator.signInWithEmailAndPassword(this.singInEmail,this.signInPassword)
      .then(value => {
        this.router.navigate(["/user-details"]);
      }).catch(reason => {
      this.displaySignInErrorMessage = true;
      this.signInError = reason.message;
      setTimeout(()=>{
        this.displaySignInErrorMessage = false;
        this.signInError = "";
      },5000);
    })
  }

  showRecoverPassword() {
    this.windowService.showSingUp = false;
    this.windowService.showSingIn = false;
    this.windowService.showVerifyOtp = false;
    this.windowService.showOtpLogin = false;
    this.windowService.showRecoverPassword = true;
  }


  recoverEmailError : string = "";
  showRecoverEmailError : boolean = false;
  showSuccessRecoverEmail : boolean = false;
  sendPasswordResetEmail() {
    this.authenticator.sendPasswordResetEmail(this.recoverEmail)
      .then(() => {
        this.showSuccessRecoverEmail = true;
      })
      .catch((error) => {
        this.showRecoverEmailError = true;
        this.recoverEmailError = error.message;
        setTimeout(()=>{
          this.showRecoverEmailError = false;
          this.recoverEmailError = "";
        },5000);
      });

  }
}
