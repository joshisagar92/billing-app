import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinginComponentComponent } from './singin-component.component';

describe('SinginComponentComponent', () => {
  let component: SinginComponentComponent;
  let fixture: ComponentFixture<SinginComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinginComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinginComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
