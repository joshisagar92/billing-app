package com.eyedyll.users;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@RunWith(SpringRunner.class)
@Testcontainers(disabledWithoutDocker = true)
public class UserControllerShould {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Container
    static PostgreSQLContainer<?> db = new PostgreSQLContainer<>("postgres:12.3");


    @DynamicPropertySource
    static void postgreSQLProperties(DynamicPropertyRegistry registry) {
        String payload = createAdminPayload();

        registry.add("spring.datasource.url", db::getJdbcUrl);
        registry.add("spring.datasource.username", db::getUsername);
        registry.add("spring.datasource.password", db::getPassword);

    }

    @Test
    public void saveAdministrator() throws Exception {
        String payload = createAdminPayload();

        this.mockMvc.perform(post("/administrator")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(payload)
                )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.size()", is(1)));
    }

    @Test
    @Sql(scripts = "/scripts/INSERT_ADMINISTRATOR.sql")
    public void getAdministrator() throws Exception {
        mockMvc.perform(get("/administrator/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstname", is("firstname")))
                .andExpect(jsonPath("$.lastname", is("lastname")))
                .andExpect(jsonPath("$.mobileno", is(1234567890)))
                .andExpect(jsonPath("$.companyname", is("companyname")))
                .andExpect(jsonPath("$.email", is("email@domian.com")))
                .andExpect(jsonPath("$.gstn", is("gstn")))
                .andExpect(jsonPath("$.addressline1", is("addressline1")))
                .andExpect(jsonPath("$.addressline2", is("addressline2")))
                .andExpect(jsonPath("$.district", is("district")))
                .andExpect(jsonPath("$.state", is("state")))
                .andExpect(jsonPath("$.zipcode", is("zipcode")));
    }

    @Test
    @Sql(scripts = "/scripts/INSERT_ADMINISTRATOR.sql")
    public void deleteAdministrator() throws Exception {
        String payload = createAdminPayload();

        mockMvc.perform(delete("/administrator")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(payload)
        )
                .andExpect(status().isGone());

    }


    private static String createAdminPayload() {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode statistic = objectMapper.createObjectNode();
        statistic.put("firstname", "firstname");
        statistic.put("lastname", "lastname");
        statistic.put("mobileno", 1234567890);
        statistic.put("companyname", "companyname");
        statistic.put("email", "email@domian.com");
        statistic.put("companyname", "companyname");
        statistic.put("gstn", "gstn");
        statistic.put("addressline1", "addressline1");
        statistic.put("addressline2", "addressline2");
        statistic.put("state", "state");
        statistic.put("zipcode", "zipcode");

        return statistic.toPrettyString();
    }
}
