package com.eyedyll.users.service;

import com.eyedyll.users.entity.User;
import com.eyedyll.users.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceShould {

    @Mock
    UserRepository userRepository;

    UserService anUserService;
    private static final int ONE = 1;

    @BeforeEach
    void setUp() {
        anUserService = new UserService(userRepository);
    }

    @Test
    void callRepositoryToSaveAdmin() {
        User user = new User();
        anUserService.save(user);
        verify(userRepository,times(ONE)).save(user);
    }

    @Test
    void callRepositoryToFindAdminById() {
        long adminId = 10;
        Optional optional = Optional.of(new User());
        when(userRepository.findById(adminId)).thenReturn(optional);
        anUserService.findWithId(adminId);
        verify(userRepository,times(ONE)).findById(adminId);
    }

    @Test
    void throwExceptionWhenAdminNotFoundById() {
        long adminId = 0;
        when(userRepository.findById(adminId)).thenThrow(NoSuchElementException.class);
        Assertions.assertThrows(NoSuchElementException.class, () -> {
             anUserService.findWithId(adminId);
        });
    }

    @Test
    void callRepositoryToDeleteAdmin() {
        User user = new User();
        anUserService.delete(user);
        verify(userRepository).delete(user);
    }
}
