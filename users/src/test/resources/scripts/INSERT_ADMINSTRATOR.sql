create table administrator (
  id int(8) not null,
  address_line1 varchar(255),
  address_line2 varchar(255),
  district varchar(255),
  firstname varchar(255),
  gstn varchar(255),
  lastname varchar(255),
  mobileno int8,
  state varchar(255),
  zipcode varchar(255),
  primary key (id)
);