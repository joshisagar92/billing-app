package com.eyedyll.users.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberValidator implements ConstraintValidator<Phone, String> {
   public void initialize(Phone constraint) {
   }

   public boolean isValid(String phoneNumber, ConstraintValidatorContext context) {
      Pattern pattern = Pattern.compile("^\\d{10}$");
      Matcher matcher = pattern.matcher(phoneNumber);
      return matcher.matches();
   }
}
