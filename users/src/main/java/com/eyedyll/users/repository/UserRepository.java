package com.eyedyll.users.repository;

import com.eyedyll.users.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
}
