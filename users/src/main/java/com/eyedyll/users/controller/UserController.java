package com.eyedyll.users.controller;


import com.eyedyll.users.entity.User;
import com.eyedyll.users.repository.UserRepository;
import com.eyedyll.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class UserController {

    UserService anUserService;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.anUserService = new UserService(userRepository);
    }

    @PostMapping("/users")
    public ResponseEntity<User> save(@RequestBody User user){
            User result = anUserService.save(user);
            ResponseEntity<User> responseEntity = new ResponseEntity<>(result,CREATED);
        return responseEntity;
    }

    @GetMapping(
            value = "/users/{id}",
            produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<User> fetch(@NonNull @Valid @PathVariable("id") Long id){
        User result = anUserService.findWithId(id);
        ResponseEntity<User> responseEntity = new ResponseEntity<>(result,OK);
        return responseEntity;
    }

    @DeleteMapping(
            value = "/users",
            consumes = APPLICATION_JSON_VALUE

    )
    public ResponseEntity<User> delete(@Valid @RequestBody User user){
        anUserService.delete(user);
        ResponseEntity<User> responseEntity = new ResponseEntity<>(GONE);
        return responseEntity;
    }

}
