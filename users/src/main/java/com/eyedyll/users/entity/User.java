package com.eyedyll.users.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import static javax.persistence.GenerationType.*;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private String firstname;
    private String lastname ;
    private String mobileno;
    private String email;
    private String location;
    private String addressLine1;
    private String addressLine2;
    private String town;
    private String postcode;
    private String country;
    private String noOfCars;
    private int publicTransportTimes;
    private int taxiTransportTimes;
    private int airTransportTimes;
    private String electricitySupplier;
    private String gasSupplier;
    private boolean buildingGeneratingElectricity;
    private String eatingHabits;
    private int nonVegFrequency;
    private int onlineShopFrequency;
    private int petFrequency;
    private String registrationNumbers;

}
