package com.eyedyll.users.service;


import com.eyedyll.users.entity.User;
import com.eyedyll.users.repository.UserRepository;

import javax.validation.Valid;
import java.util.Optional;

public class UserService {

    private UserRepository anUserRepository;

    public UserService(UserRepository anUserRepository) {
        this.anUserRepository = anUserRepository;
    }

    public User save(User user) {
        User result = anUserRepository.save(user);
        return result;
    }

    public User findWithId(Long id) {
        Optional<User> byId = anUserRepository.findById(id);
        User user = byId.orElseThrow();
        return user;
    }

    public void delete(@Valid User user) {
        anUserRepository.delete(user);
    }
}
