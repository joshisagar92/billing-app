package com.vehicle.auth.adminstration.intializer;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import com.vehicle.auth.adminstration.stubs.AuthenticationStub;
import com.vehicle.auth.adminstration.stubs.StubFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextClosedEvent;


public class WireMockIntializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {


    private static final Logger LOG = LoggerFactory.getLogger(WireMockIntializer.class);

    private AuthenticationStub authenticationStub;

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        LOG.info("About to start wiremock server");
        WireMockServer wireMockServer = new WireMockServer(new WireMockConfiguration().port(8090));
        wireMockServer.start();
        StubFactory.setAuthenticationStub(new AuthenticationStub(wireMockServer));

        LOG.info("Wiremock successfully started");
        applicationContext.getBeanFactory().registerSingleton("wireMockServer",wireMockServer);

        applicationContext.addApplicationListener(applicationEvent -> {

            if(applicationEvent instanceof ContextClosedEvent){
                wireMockServer.stop();
                LOG.info("Wiremock successfully stopped");
            }

        });

        TestPropertyValues
                .of("appplication.authentication.api.url=http://localhost:" + wireMockServer.port() + "/thirdparty-access/v1")
                .applyTo(applicationContext);
        TestPropertyValues
                .of("appplication.vehicle-enquary.api.url=http://localhost:" + wireMockServer.port() + "/vehicle-enquiry/v1")
                .applyTo(applicationContext);
    }
}
