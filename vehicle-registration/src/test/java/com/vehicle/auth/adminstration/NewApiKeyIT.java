package com.vehicle.auth.adminstration;

import com.vehicle.auth.adminstration.controller.AuthAdminController;
import com.vehicle.auth.adminstration.domain.ApiKeyCredential;
import com.vehicle.auth.adminstration.domain.ApiKeyResponse;
import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.intializer.WireMockIntializer;
import com.vehicle.auth.adminstration.stubs.AuthenticationStub;
import com.vehicle.auth.adminstration.stubs.StubFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
@ActiveProfiles("acceptance-test")
@ContextConfiguration(initializers = WireMockIntializer.class)
public class NewApiKeyIT {

    private String username;
    private String password;
    private String apiKey;

    private AuthenticationStub stubInstance;

    @Autowired
    private AuthAdminController controller;


    @BeforeEach
    void setUp() {
        username = "username";
        password="password";
        apiKey="abc-bcd";
        stubInstance = StubFactory.getAuthenticationStubInstance();
    }

    @Test
    void should_get_new_api_key() throws IOException, InterruptedException {

        String idToken = "abc-jkl";
        stubInstance.createAuthenticateSuccessStub(username,password, idToken);
        stubInstance.createNewApiKeySuccess(idToken,apiKey);
        ApiKeyCredential userCredential = new ApiKeyCredential(username,password,apiKey);
        Response<ApiKeyResponse> newApiKey = controller.getNewApiKey(userCredential);
        ApiKeyResponse apiKeyResponse = newApiKey.getType();
        assertThat(newApiKey.getStatus().value(),is(200));
        assertThat(apiKeyResponse.getMessage(),is("API Key successfully changed."));
        assertThat(apiKeyResponse.getNewApiKey(),is("XYZ-ABC"));
    }

    @Test
    void should_throw_bad_request_when_user_input_is_invalid() throws IOException, InterruptedException {
        String idToken = "abc-jkl";
        int statuscode = 401;
        String title = "Unauthorized";
        String detail = "API Key or JWT is either not provided, expired or invalid.";
        stubInstance.createAuthenticateSuccessStub(username,password, idToken);
        stubInstance.createNewApiKeyError(statuscode,title,detail, apiKey, idToken);
        ApiKeyCredential userCredential = new ApiKeyCredential(username,password,apiKey);
        Response<ApiKeyResponse> newApiKey = controller.getNewApiKey(userCredential);
        ApiKeyResponse apiKeyResponse = newApiKey.getType();
        assertThat(apiKeyResponse.getTitle(),is(title));
        assertThat(newApiKey.getStatus().value(),is(statuscode));
        assertThat(apiKeyResponse.getDetail(),is(detail));

    }
}
