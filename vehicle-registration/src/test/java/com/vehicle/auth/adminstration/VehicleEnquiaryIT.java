package com.vehicle.auth.adminstration;

import com.vehicle.auth.adminstration.controller.AuthAdminController;
import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.domain.VehicleEnquiryRequest;
import com.vehicle.auth.adminstration.entity.RegistrationDetailsDto;
import com.vehicle.auth.adminstration.intializer.WireMockIntializer;
import com.vehicle.auth.adminstration.stubs.AuthenticationStub;
import com.vehicle.auth.adminstration.stubs.StubFactory;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static org.hamcrest.CoreMatchers.is;

@SpringBootTest
@ActiveProfiles("acceptance-test")
@ContextConfiguration(initializers = WireMockIntializer.class)
public class VehicleEnquiaryIT {


    @Autowired
    AuthAdminController vehicleEnquiryController;
    AuthenticationStub stubInstance;

    @BeforeEach
    void setUp() {
        stubInstance = StubFactory.getAuthenticationStubInstance();

    }

    @Test
    void testVehicleEnquiry() {
        String apiKey = "ABC-XYZ";
        String registrationNo = "ABC1234";
        VehicleEnquiryRequest request = new VehicleEnquiryRequest(registrationNo, apiKey);
        stubInstance.vehicleRegistrationSuccess(apiKey,registrationNo);
        Response<RegistrationDetailsDto> details = vehicleEnquiryController.getDetails(request);
        RegistrationDetailsDto registrationDetails = details.getType();
        MatcherAssert.assertThat(registrationDetails.getArtEndDate(),is("2025-02-28"));
    }

    @Test
    void testVehicleEnquiryError() {

        int status=400;
        String code ="ENQ103";
        String title="Bad Request";
        String detail="Invalid format for field - vehicle registration number";
        String apiKey = "ABC-XYZ";
        String registrationNo = "ABC1234";
        VehicleEnquiryRequest request = new VehicleEnquiryRequest(registrationNo, apiKey);
        stubInstance.vehicleRegistrationError(apiKey,registrationNo,status,code,title,detail);
        Response<RegistrationDetailsDto> details = vehicleEnquiryController.getDetails(request);
        RegistrationDetailsDto registrationDetails = details.getType();
        MatcherAssert.assertThat(registrationDetails.getTitle(),is(title));
        MatcherAssert.assertThat(registrationDetails.getCode(),is(code));
        MatcherAssert.assertThat(registrationDetails.getDetail(),is(detail));
    }
}
