package com.vehicle.auth.adminstration;

import com.vehicle.auth.adminstration.controller.AuthAdminController;
import com.vehicle.auth.adminstration.domain.JWTToken;
import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.domain.UserCredential;
import com.vehicle.auth.adminstration.intializer.WireMockIntializer;
import com.vehicle.auth.adminstration.stubs.AuthenticationStub;
import com.vehicle.auth.adminstration.stubs.StubFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("acceptance-test")
@ContextConfiguration(initializers = WireMockIntializer.class)
class AuthTokenIT {
    private AuthenticationStub stubInstance;

    @Autowired
    AuthAdminController controller;

    @BeforeEach
    void setUp() {
        stubInstance = StubFactory.getAuthenticationStubInstance();
    }

    @Test
    void should_fetch_auth_token_when_correct_username_password_is_given() throws IOException, InterruptedException {
        String user = "user";
        String password = "password";
        String expectedToken = "abc-jkl";
        stubInstance.createAuthenticateSuccessStub(user, password, expectedToken);
        UserCredential userCredential = new UserCredential(user,password);

        Response<JWTToken> response = controller.getJwtToken(userCredential);
        JWTToken jwtToken =  response.getType();
        String token = jwtToken.getIdToken();

        assertThat(token,is(expectedToken));
        assertThat(response.getStatus().value(),is(200));
     }

    @Test
    void should_return_bad_request_when_request_body_is_malformed() throws IOException, InterruptedException {
        String user = "user";
        stubInstance.createAuthenticateBadRequestStub(user);
        UserCredential userCredential = new UserCredential(user,"");

        Response<JWTToken> response = controller.getJwtToken(userCredential);
        JWTToken jwtToken =  response.getType();

        assertThat(jwtToken.getTitle(),is("Malformed request"));
        assertThat(response.getStatus().value(),is(400));
        assertNull(jwtToken.getIdToken());
        assertNull(jwtToken.getDetail());
    }

    @Test
    void should_return_authentication_failure_when_request_body_contains_wrong_user_name_or_password() throws IOException, InterruptedException {
        String user = "user";
        String password = "password";
        String expectedDetails = "Supplied username or password was incorrect, or too many incorrect attempts have been made.";
        stubInstance.createAuthenticateWrongUsernamePasswordstub(user,password);
        UserCredential userCredential = new UserCredential(user,password);

        Response<JWTToken> response = controller.getJwtToken(userCredential);
        JWTToken jwtToken =  response.getType();

        assertThat(jwtToken.getTitle(),is("Authentication Failure"));
        assertThat(response.getStatus().value(),is(401));
        assertThat(jwtToken.getDetail(),is(expectedDetails));
        assertNull(jwtToken.getIdToken());


    }


    @Test
    void should_return_authentication_failure_when_user_uses_temporary_password() throws IOException, InterruptedException {
        String user = "user";
        String password = "password";
        String expectedDetails = "Please change your temporary password to verify your account.";
        stubInstance.createAuthenticateTemporaryPasswordstub(user,password);
        UserCredential userCredential = new UserCredential(user,password);

        Response<JWTToken> response = controller.getJwtToken(userCredential);
        JWTToken jwtToken =  response.getType();

        assertThat(jwtToken.getTitle(),is("Authentication Failure"));
        assertThat(response.getStatus().value(),is(401));
        assertThat(jwtToken.getDetail(),is(expectedDetails));
        assertNull(jwtToken.getIdToken());
    }
}
