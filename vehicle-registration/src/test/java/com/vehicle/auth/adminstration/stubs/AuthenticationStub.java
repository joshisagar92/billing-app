package com.vehicle.auth.adminstration.stubs;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


public class AuthenticationStub {

    public  WireMockServer wireMockServer;

    public AuthenticationStub(WireMockServer wireMockServer) {
        this.wireMockServer = wireMockServer;
    }

    public void createAuthenticateSuccessStub(String username, String password, String idToken){

        JSONObject body = new JSONObject();
        JSONObject JWTToken = new JSONObject();
        try {
            body.put("userName",username);
            body.put("password",password);
            JWTToken.put("id-token",idToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        wireMockServer.stubFor(
                post("/thirdparty-access/v1/authenticate")
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(aResponse()
                                .withBody(JWTToken.toString())
                                .withHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE))
                        .withHeader(HttpHeaders.ACCEPT_CHARSET,equalTo(String.valueOf(UTF_8)))
        );

    }

    public void createAuthenticateBadRequestStub(String username){
        JSONObject body = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        JSONArray response = new JSONArray();
        try {
            body.put("userName",username);
            body.put("password","");
            jsonObject.put("status", 400);
            jsonObject.put("title","Malformed request");
            response.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

         wireMockServer.stubFor(
                post("/thirdparty-access/v1/authenticate")
                .withRequestBody(equalToJson(body.toString()))
                .willReturn(badRequest().withStatus(400).withBody(response.toString())));
    }

    public void createAuthenticateWrongUsernamePasswordstub(String username,String password) {
        JSONObject body = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        JSONArray response = new JSONArray();
        try {
            body.put("userName",username);
            body.put("password",password);
            jsonObject.put("status", 401);
            jsonObject.put("title","Authentication Failure");
            jsonObject.put("detail","Supplied username or password was incorrect, or too many incorrect attempts have been made.");
            response.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post("/thirdparty-access/v1/authenticate")
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(badRequest().withStatus(401).withBody(response.toString())));
    }

    public void createAuthenticateTemporaryPasswordstub(String username, String password) {
        JSONObject body = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        JSONArray response = new JSONArray();
        try {
            body.put("userName",username);
            body.put("password",password);
            jsonObject.put("status", 401);
            jsonObject.put("title","Authentication Failure");
            jsonObject.put("detail","Please change your temporary password to verify your account.");
            response.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post("/thirdparty-access/v1/authenticate")
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(badRequest().withStatus(401).withBody(response.toString())));
    }

    public void changePasswordSuccessStub(String username, String password, String newpassword) {
        JSONObject body = new JSONObject();
        JSONObject JWTToken = new JSONObject();
        try {
            body.put("userName",username);
            body.put("password",password);
            body.put("newPassword",newpassword);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post("/thirdparty-access/v1/password")
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(aResponse().withStatus(200)));

    }


    public void changePasswordErrorStub(int statuscode, String title, String detail) {
        JSONObject body = new JSONObject();
        try {
            body.put("userName","username");
            body.put("password","password");
            body.put("newPassword","newpassword");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ErrorStub(statuscode, title, detail, "/thirdparty-access/v1/password",body);
    }

    public void newPasswordSuccessStub(String username, String email) {
        JSONObject body = new JSONObject();
        JSONObject response = new JSONObject();
        try {
            body.put("userName",username);
            body.put("email",email);
            response.put("message","Password reset code sent.");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post("/thirdparty-access/v1/new-password")
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(aResponse().withStatus(200).withBody(response.toString())));

    }

    public void newPasswordErrorStub(int statuscode, String title, String detail) {

        JSONObject body = new JSONObject();
        try {
            body.put("userName","username");
            body.put("email","username@email.com");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ErrorStub(statuscode, title, detail, "/thirdparty-access/v1/new-password", body);
    }


    public void createNewApiKeySuccess(String idToken, String apiKey) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("message","API Key successfully changed.");
            jsonObject.put("newApiKey","XYZ-ABC");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        wireMockServer.stubFor(
                post("/thirdparty-access/v1/new-api-key")
                        .withHeader("x-api-key",equalTo(apiKey))
                        .withHeader("Authorization",equalTo(idToken))
                        .willReturn(aResponse().withBody(jsonObject.toString())));

    }

    public void createNewApiKeyError(int statuscode, String title, String detail, String apiKey, String idToken) {
        JSONObject jsonObject = new JSONObject();
        JSONArray response = new JSONArray();
        try {

            jsonObject.put("status", statuscode);
            jsonObject.put("title",title);
            jsonObject.put("detail",detail);
            response.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post("/thirdparty-access/v1/new-api-key")
                        .withHeader("x-api-key",equalTo(apiKey))
                        .withHeader("Authorization",equalTo(idToken))
                        .willReturn(badRequest().withStatus(statuscode).withBody(response.toString())));

    }
    private void ErrorStub(int statuscode, String title, String detail, String url, JSONObject body) {
        JSONObject jsonObject = new JSONObject();
        JSONArray response = new JSONArray();
        try {

            jsonObject.put("status", statuscode);
            jsonObject.put("title",title);
            jsonObject.put("detail",detail);
            response.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post(url)
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(badRequest().withStatus(statuscode).withBody(response.toString())));
    }


    public void vehicleRegistrationSuccess(String apiKey,String registrationNo) {
        JSONObject body = new JSONObject();
        JSONObject response = new JSONObject();

        try {
        response.put("artEndDate","2025-02-28");
        response.put("co2Emissions",135);
        response.put("colour","BLUE");
        response.put("engineCapacity",2494);
        response.put("fuelType","PETROL");
        response.put("make","ROVER");
        response.put("fuelType","PETROL");
        response.put("fuelType","PETROL");
        response.put("markedForExport",false);
        response.put("monthOfFirstRegistration","2004-12");
        response.put("registrationNumber","ABC1234");
        response.put("revenueWeight",1640);
        response.put("taxDueDate","2007-01-01");
        response.put("taxStatus","N1");
        response.put("typeApproval","Untaxed");
        response.put("wheelplan","NON STANDARD");
        response.put("yearOfManufacture",2004);
        response.put("euroStatus","URO 6 AD");
            response.put("realDrivingEmissions",1);
        response.put("dateOfLastV5CIssued","2016-12-25");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            body.put("registrationNumber",registrationNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        wireMockServer.stubFor(
                post("/vehicle-enquiry/v1/vehicles")
                        .withHeader("x-api-key",equalTo(apiKey))
                        .withHeader(HttpHeaders.CONTENT_TYPE,equalTo(APPLICATION_JSON_VALUE))
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(aResponse().withBody(response.toString())));
    }



    public void vehicleRegistrationError(String apiKey, String registrationNo, int status, String code, String title, String detail) {
        JSONObject body = new JSONObject();
        JSONObject response = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject object = new JSONObject();
        try {
            response.put("status",status);
            response.put("code",code);
            response.put("title",title);
            response.put("detail",detail);
            array.put(response);

            object.put("errors",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            body.put("registrationNumber",registrationNo);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        wireMockServer.stubFor(
                post("/vehicle-enquiry/v1/vehicles")
                        .withHeader("x-api-key",equalTo(apiKey))
                        .withHeader(HttpHeaders.CONTENT_TYPE,equalTo(APPLICATION_JSON_VALUE))
                        .withRequestBody(equalToJson(body.toString()))
                        .willReturn(badRequest().withStatus(400).withBody(object.toString())));
    }
}
