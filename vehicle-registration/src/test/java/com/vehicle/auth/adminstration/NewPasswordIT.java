package com.vehicle.auth.adminstration;


import com.vehicle.auth.adminstration.controller.AuthAdminController;
import com.vehicle.auth.adminstration.domain.*;
import com.vehicle.auth.adminstration.intializer.WireMockIntializer;
import com.vehicle.auth.adminstration.stubs.AuthenticationStub;
import com.vehicle.auth.adminstration.stubs.StubFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
@ActiveProfiles("acceptance-test")
@ContextConfiguration(initializers = WireMockIntializer.class)
public class NewPasswordIT {

    private String username;
    private String email;

    private AuthenticationStub stubInstance;

    @Autowired
    private AuthAdminController controller;

    @BeforeEach
    void setUp() {
        username = "username";
        email="username@email.com";
        stubInstance = StubFactory.getAuthenticationStubInstance();
    }

    @Test
    void should_return_verification_code_in_email_when_correct_username_and_email_provided() throws IOException, InterruptedException {
        String message = "Password reset code sent.";
        stubInstance.newPasswordSuccessStub(username,email);
        NewPassword newPassword = new NewPassword(username,email);
        Response<NewPasswordResponse> response = controller.getNewPassword(newPassword);
        NewPasswordResponse newPasswordResponse = response.getType();
        assertThat(response.getStatus().value(),is(200));
        assertThat(newPasswordResponse.getMessage(),is(message));
    }

    @Test
    void should_return_unauthorized_access_when_invalid_input_given() throws IOException, InterruptedException {
        int statuscode = 400;
        String title = "Invalid Request";
        String detail = "New Password does not require API Key";
        stubInstance.newPasswordErrorStub(statuscode, title, detail);
        NewPassword newPassword = new NewPassword(username,email);
        Response<NewPasswordResponse> response = controller.getNewPassword(newPassword);

        NewPasswordResponse passwordResponse = response.getType();

        assertThat(passwordResponse.getTitle(),is(title));
        assertThat(response.getStatus().value(),is(statuscode));
        assertThat(passwordResponse.getDetail(),is(detail));
    }

    @Test
    void should_return_internal_server_error_when_api_has_an_issue() throws IOException, InterruptedException {
        int statuscode = 500;
        String title = "Internal Server Error";
        String detail = "";
        stubInstance.newPasswordErrorStub(statuscode, title, detail);
        NewPassword newPassword = new NewPassword(username,email);
        Response<NewPasswordResponse> response = controller.getNewPassword(newPassword);

        NewPasswordResponse passwordResponse = response.getType();
        assertThat(passwordResponse.getTitle(),is(title));
        assertThat(response.getStatus().value(),is(statuscode));
    }
}
