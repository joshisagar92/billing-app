package com.vehicle.auth.adminstration.stubs;

public class StubFactory {


    private static AuthenticationStub authenticationStub;

    public static AuthenticationStub getAuthenticationStubInstance(){
        return authenticationStub;
    }

    public static void setAuthenticationStub(AuthenticationStub stub){
        authenticationStub = stub;
    }
}
