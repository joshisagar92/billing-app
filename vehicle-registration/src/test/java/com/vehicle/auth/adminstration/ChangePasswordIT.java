package com.vehicle.auth.adminstration;

import com.vehicle.auth.adminstration.controller.AuthAdminController;
import com.vehicle.auth.adminstration.domain.ChangePasswordCredential;
import com.vehicle.auth.adminstration.domain.Password;
import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.intializer.WireMockIntializer;
import com.vehicle.auth.adminstration.stubs.AuthenticationStub;
import com.vehicle.auth.adminstration.stubs.StubFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;



@SpringBootTest
@ActiveProfiles("acceptance-test")
@ContextConfiguration(initializers = WireMockIntializer.class)
public class ChangePasswordIT {

    private String username;
    private String password;
    private String newpassword;
    private AuthenticationStub stubInstance;

    @Autowired
    private AuthAdminController controller;


    @BeforeEach
    void setUp() {
        username = "username";
        password = "password";
        newpassword = "newpassword";
        stubInstance = StubFactory.getAuthenticationStubInstance();
    }

    @Test
    void should_return_status_code_200_with_no_body_when_password_successfully_changed() throws IOException, InterruptedException {
        stubInstance.changePasswordSuccessStub(username,password,newpassword);
        ChangePasswordCredential credential = new ChangePasswordCredential(username,password,newpassword);
        Response<Password> response = controller.changePassword(credential);
        assertThat(response.getStatus().value(),is(200));
     }

    @Test
    void should_throw_bad_request_when_parameter_is_invalid_or_missing() throws IOException, InterruptedException {
        int statuscode = 400;
        String title = "Invalid Request";
        String detail = "Please supply userName, newPassword and either password or verifyCode";
        stubInstance.changePasswordErrorStub(statuscode, title, detail);
        ChangePasswordCredential credential = new ChangePasswordCredential(username,password,newpassword);
        Response<Password> response = controller.changePassword(credential);

        Password password = response.getType();

        assertThat(password.getTitle(),is(title));
        assertThat(response.getStatus().value(),is(statuscode));
        assertThat(password.getDetail(),is(detail));
     }


    @Test
    void should_return_unauthorized_access_when_usernaem_or_password_incorrect() throws IOException, InterruptedException {
        int statuscode = 401;
        String title = "Authentication Failure";
        String detail = "Supplied username or password was incorrect, your temporary password has expired, or too many incorrect attempts have been made. To request a new password, please make a POST API call to /new-password specifying ‘userName’ and 'email’ in the body.";
        stubInstance.changePasswordErrorStub(statuscode, title, detail);
        ChangePasswordCredential credential = new ChangePasswordCredential(username,password,newpassword);
        Response<Password> response = controller.changePassword(credential);

        Password password = response.getType();

        assertThat(password.getTitle(),is(title));
        assertThat(response.getStatus().value(),is(statuscode));
        assertThat(password.getDetail(),is(detail));
    }
}
