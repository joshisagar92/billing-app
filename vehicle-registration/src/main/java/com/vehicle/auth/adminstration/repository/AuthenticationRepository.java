package com.vehicle.auth.adminstration.repository;

import com.vehicle.auth.adminstration.domain.*;

import java.io.IOException;


public interface AuthenticationRepository {
    <T> Response<T> getToken(UserCredential userCredential) throws IOException, InterruptedException;
    <T> Response<Password> changeOrRecoverPassword(T t, Class<T> classz) throws IOException, InterruptedException;
    <T> Response<T> getNewPassword(NewPassword newPassword) throws IOException, InterruptedException;
    <T> Response<T> getApiKeys(String apiKey, JWTToken jwtToken) throws IOException, InterruptedException;
}
