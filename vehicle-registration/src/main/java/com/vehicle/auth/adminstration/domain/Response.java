package com.vehicle.auth.adminstration.domain;

import org.springframework.http.HttpStatus;

public class Response<T> {

    private String message;
    private HttpStatus status;
    private T t;

    public Response(String message, HttpStatus status, T t) {
        this.message = message;
        this.status = status;
        this.t = t;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public T getType() {
        return t;
    }

    @Override
    public String toString() {
        return "Response{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}