package com.vehicle.auth.adminstration.service;

import com.vehicle.auth.adminstration.domain.*;

import com.vehicle.auth.adminstration.repository.AuthenticationRepository;

import java.io.IOException;

public class AuthenticationService {


    private AuthenticationRepository authenticationRepository;

    public AuthenticationService(AuthenticationRepository authenticationRepository) {
        this.authenticationRepository = authenticationRepository;
    }

    public Response<JWTToken> getToken(UserCredential userCredential) throws IOException, InterruptedException {
        return authenticationRepository.getToken(userCredential);
    }

    public Response<Password> changePassword(ChangePasswordCredential changePasswordCredential) throws IOException, InterruptedException {
        return authenticationRepository.changeOrRecoverPassword(changePasswordCredential,ChangePasswordCredential.class);
    }

    public Response<Password> recoverPassword(PasswordRecoveryCredential passwordRecoveryCredential) throws IOException, InterruptedException {
       return authenticationRepository.changeOrRecoverPassword(passwordRecoveryCredential,PasswordRecoveryCredential.class);
    }

    public Response<NewPasswordResponse> getNewPassword(NewPassword newPassword) throws IOException, InterruptedException {
        return authenticationRepository.getNewPassword(newPassword);
    }

    public Response<ApiKeyResponse> getNewApiKeys(String apiKey,JWTToken jwtToken) throws IOException, InterruptedException {
        return authenticationRepository.getApiKeys(apiKey,jwtToken);
    }
}
