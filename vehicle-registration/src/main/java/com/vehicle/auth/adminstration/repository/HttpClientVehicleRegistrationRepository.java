package com.vehicle.auth.adminstration.repository;


import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vehicle.auth.adminstration.domain.RegistrationDetails;
import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.domain.VehicleEnquiryRequest;
import com.vehicle.auth.adminstration.entity.RegistrationDetailsDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Repository
public class HttpClientVehicleRegistrationRepository implements VehicleEnquiryRepository{

    @Value("${appplication.vehicle-enquary.api.url}")
    private String  BASE_URL;

    @Override
    public  Response<RegistrationDetailsDto> getDetails(VehicleEnquiryRequest vehicleEnquiryRequest) {

        JsonObject body = new JsonObject();
        body.addProperty("registrationNumber",vehicleEnquiryRequest.getRegistrationNumber());

        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(BASE_URL + "/vehicles"))
                .header("Content-Type", "application/json")
                .header("x-api-key", vehicleEnquiryRequest.getApiKey())
                .POST(HttpRequest.BodyPublishers.ofString(body.toString()))
                .build();

        HttpResponse<String> httpResponse = null;
        try {
            httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RegistrationDetailsDto registrationDetails;
        if(httpResponse.statusCode() != 200){

            JsonObject errorObject = JsonParser.parseString(httpResponse.body()).getAsJsonObject();
            JsonElement element = errorObject.get("errors");
            JsonElement errorElement = element.getAsJsonArray().get(0);

            registrationDetails = new Gson().fromJson(errorElement.toString(), RegistrationDetailsDto.class);
        }else {
            registrationDetails = new Gson().fromJson(httpResponse.body(),RegistrationDetailsDto.class);
        }
        Response<RegistrationDetailsDto> response = new Response<RegistrationDetailsDto>("", HttpStatus.valueOf(httpResponse.statusCode()),
                registrationDetails);
        return response;
    }
}
