package com.vehicle.auth.adminstration.domain;

public class NewPasswordResponse {

    private int status;
    private String title;
    private String detail;
    private String message;



    public NewPasswordResponse(int status, String title, String detail, String message) {

        this.status = status;
        this.title = title;
        this.detail = detail;
        this.message = message;
    }

    public String getDetail() {
        return detail;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "NewPasswordResponse{" +
                "status=" + status +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
