package com.vehicle.auth.adminstration.domain;

public class PasswordRecoveryCredential {

    private String userName;
    private String verifyCode;
    private String newPassword;


    public PasswordRecoveryCredential(String userName, String verifyCode, String newPassword) {
        this.userName = userName;
        this.verifyCode = verifyCode;
        this.newPassword = newPassword;
    }

    public String getUserName() {
        return userName;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public String getNewPassword() {
        return newPassword;
    }
}
