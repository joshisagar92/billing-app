package com.vehicle.auth.adminstration.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;

@Entity
public class RegistrationDetailsDto {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long Id;

    public Long getId() {
        return Id;
    }

    private String artEndDate;
    private int co2Emissions;
    private String colour;
    private int engineCapacity;
    private String fuelType;
    private String make;
    private boolean markedForExport;
    private String monthOfFirstRegistration;
    private String motStatus;
    private String registrationNumber;
    private int revenueWeight;
    private String taxDueDate;
    private String taxStatus;
    private String typeApproval;
    private String wheelplan;
    private int yearOfManufacture;
    private String euroStatus;
    private String realDrivingEmissions;
    private String dateOfLastV5CIssued;

    @JsonInclude()
    @Transient
    private String title ;

    @JsonInclude()
    @Transient
    private String detail;

    @JsonInclude()
    @Transient
    private int status   ;

    @JsonInclude()
    @Transient
    private String code  ;


    public RegistrationDetailsDto() {
    }

    public String getArtEndDate() {
        return artEndDate;
    }

    public void setArtEndDate(String artEndDate) {
        this.artEndDate = artEndDate;
    }

    public int getCo2Emissions() {
        return co2Emissions;
    }

    public void setCo2Emissions(int co2Emissions) {
        this.co2Emissions = co2Emissions;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public boolean isMarkedForExport() {
        return markedForExport;
    }

    public void setMarkedForExport(boolean markedForExport) {
        this.markedForExport = markedForExport;
    }

    public String getMonthOfFirstRegistration() {
        return monthOfFirstRegistration;
    }

    public void setMonthOfFirstRegistration(String monthOfFirstRegistration) {
        this.monthOfFirstRegistration = monthOfFirstRegistration;
    }

    public String getMotStatus() {
        return motStatus;
    }

    public void setMotStatus(String motStatus) {
        this.motStatus = motStatus;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getRevenueWeight() {
        return revenueWeight;
    }

    public void setRevenueWeight(int revenueWeight) {
        this.revenueWeight = revenueWeight;
    }

    public String getTaxDueDate() {
        return taxDueDate;
    }

    public void setTaxDueDate(String taxDueDate) {
        this.taxDueDate = taxDueDate;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public String getTypeApproval() {
        return typeApproval;
    }

    public void setTypeApproval(String typeApproval) {
        this.typeApproval = typeApproval;
    }

    public String getWheelplan() {
        return wheelplan;
    }

    public void setWheelplan(String wheelplan) {
        this.wheelplan = wheelplan;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getEuroStatus() {
        return euroStatus;
    }

    public void setEuroStatus(String euroStatus) {
        this.euroStatus = euroStatus;
    }

    public String getRealDrivingEmissions() {
        return realDrivingEmissions;
    }

    public void setRealDrivingEmissions(String realDrivingEmissions) {
        this.realDrivingEmissions = realDrivingEmissions;
    }

    public String getDateOfLastV5CIssued() {
        return dateOfLastV5CIssued;
    }

    public void setDateOfLastV5CIssued(String dateOfLastV5CIssued) {
        this.dateOfLastV5CIssued = dateOfLastV5CIssued;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "RegistrationDetailsDto{" +
                "Id=" + Id +
                ", artEndDate='" + artEndDate + '\'' +
                ", co2Emissions=" + co2Emissions +
                ", colour='" + colour + '\'' +
                ", engineCapacity=" + engineCapacity +
                ", fuelType='" + fuelType + '\'' +
                ", make='" + make + '\'' +
                ", markedForExport=" + markedForExport +
                ", monthOfFirstRegistration='" + monthOfFirstRegistration + '\'' +
                ", motStatus='" + motStatus + '\'' +
                ", registrationNumber='" + registrationNumber + '\'' +
                ", revenueWeight=" + revenueWeight +
                ", taxDueDate='" + taxDueDate + '\'' +
                ", taxStatus='" + taxStatus + '\'' +
                ", typeApproval='" + typeApproval + '\'' +
                ", wheelplan='" + wheelplan + '\'' +
                ", yearOfManufacture=" + yearOfManufacture +
                ", euroStatus='" + euroStatus + '\'' +
                ", realDrivingEmissions='" + realDrivingEmissions + '\'' +
                ", dateOfLastV5CIssued='" + dateOfLastV5CIssued + '\'' +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                ", status=" + status +
                ", code='" + code + '\'' +
                '}';
    }
}
