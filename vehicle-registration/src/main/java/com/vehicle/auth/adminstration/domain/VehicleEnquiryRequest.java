package com.vehicle.auth.adminstration.domain;

public class VehicleEnquiryRequest {

    private String registrationNumber;
    private String apiKey;

    public VehicleEnquiryRequest(String registrationNumber, String apiKey) {
        this.registrationNumber = registrationNumber;
        this.apiKey = apiKey;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public String getApiKey() {
        return apiKey;
    }

    @Override
    public String toString() {
        return "VehicleEnquiryRequest{" +
                "registrationNumber='" + registrationNumber + '\'' +
                ", apiKey='" + apiKey + '\'' +
                '}';
    }
}
