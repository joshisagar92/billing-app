package com.vehicle.auth.adminstration.controller;


import com.vehicle.auth.adminstration.domain.*;
import com.vehicle.auth.adminstration.entity.RegistrationDetailsDto;
import com.vehicle.auth.adminstration.repository.HttpClientVehicleRegistrationRepository;
import com.vehicle.auth.adminstration.repository.VehicleRegistrationRepository;
import com.vehicle.auth.adminstration.repository.WebClientAuthenticationRepository;
import com.vehicle.auth.adminstration.service.AuthenticationService;
import com.vehicle.auth.adminstration.service.VehicleEnquiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController("authentication")
public class AuthAdminController {

    private AuthenticationService authenticationService;
    private VehicleEnquiryService vehicleEnquiryService;


    @Autowired
    public AuthAdminController(WebClientAuthenticationRepository authenticationRepository, HttpClientVehicleRegistrationRepository repository, VehicleRegistrationRepository registrationRepository) {

        this.authenticationService = new AuthenticationService(authenticationRepository);
        this.vehicleEnquiryService = new VehicleEnquiryService(repository, registrationRepository);
    }

    @GetMapping(value = "/jwt-token",produces = "application/json")
    public Response<JWTToken> getJwtToken(@RequestBody UserCredential userCredential) throws IOException, InterruptedException {
        Response<JWTToken> token = authenticationService.getToken(userCredential);
        return token;
    }

    @PostMapping(value = "/change-password",produces = "application/json")
    public Response<Password> changePassword(@RequestBody ChangePasswordCredential changePasswordCredential) throws IOException, InterruptedException {
        return authenticationService.changePassword(changePasswordCredential);
    }

    @PostMapping(value = "/recover-password",produces = "application/json")
    public Response<Password> changePassword(@RequestBody PasswordRecoveryCredential passwordRecoveryCredential) throws IOException, InterruptedException {
        return authenticationService.recoverPassword(passwordRecoveryCredential);
    }

    @PostMapping(value = "/new-password",produces = "application/json")
    public Response<NewPasswordResponse> getNewPassword(@RequestBody NewPassword newPassword) throws IOException, InterruptedException {
        return authenticationService.getNewPassword(newPassword);
    }

    @PostMapping(value = "/new-api-key",produces = "application/json")
    public Response<ApiKeyResponse> getNewApiKey(@RequestBody ApiKeyCredential apiKeyCredential) throws IOException, InterruptedException {
        UserCredential userCredential = new UserCredential(apiKeyCredential.getUsername()
                ,apiKeyCredential.getPassword());
        JWTToken jwtToken = getJwtToken(userCredential).getType();
        return authenticationService.getNewApiKeys(apiKeyCredential.getApiKey(),jwtToken);
    }

    @PostMapping(value = "/registration-details",produces = "application/json")
    public Response<RegistrationDetailsDto> getDetails(@RequestBody VehicleEnquiryRequest vehicleEnquiryRequest){
        /*RegistrationDetails registrationDetails = vehicleEnquiryService.findByRegistrationNumber(vehicleEnquiryRequest.getRegistrationNumber());
        String registrationNumber = registrationDetails.getRegistrationNumber();
        if(registrationNumber !=null && !"".equals(registrationNumber)){
            Response<RegistrationDetails> response = new Response<>("", HttpStatus.OK,registrationDetails);
            return response;
        }*/

        return vehicleEnquiryService.getDetails(vehicleEnquiryRequest);
    }
}
