package com.vehicle.auth.adminstration.domain;

public class Password {

    private int status;
    private String title;
    private String detail;


    public Password() {
    }

    public Password(int status, String title, String detail) {

        this.status = status;
        this.title = title;
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }


    public int getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "JWTToken{" +
                ", status=" + status +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }

}
