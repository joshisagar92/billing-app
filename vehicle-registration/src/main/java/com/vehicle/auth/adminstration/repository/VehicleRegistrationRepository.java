package com.vehicle.auth.adminstration.repository;

import com.vehicle.auth.adminstration.entity.RegistrationDetailsDto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VehicleRegistrationRepository extends CrudRepository<RegistrationDetailsDto,Long> {
    List<RegistrationDetailsDto> findByRegistrationNumber(String registrationNumber);
}
