package com.vehicle.auth.adminstration.domain;

public class JWTToken {
    private  String string;
    private int status;
    private String title;
    private String detail;


    public JWTToken(String idToken, int status, String title, String detail) {
        this.string = idToken;
        this.status = status;
        this.title = title;
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public String getIdToken() {
        return string;
    }

    public int getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "JWTToken{" +
                "string='" + string + '\'' +
                ", status=" + status +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
