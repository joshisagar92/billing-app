package com.vehicle.auth.adminstration.domain;

public class UserCredential {

    private String userName;
    private String password;

    public UserCredential(String username, String password) {
        this.userName = username;
        this.password = password;
    }

    public String getUsername() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "{" +
                "userName:'" + userName + '\'' +
                ", password:'" + password + '\'' +
                '}';
    }
}
