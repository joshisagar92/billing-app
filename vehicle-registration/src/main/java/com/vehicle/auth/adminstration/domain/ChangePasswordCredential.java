package com.vehicle.auth.adminstration.domain;

public class ChangePasswordCredential {
    private String userName;
    private String password;
    private String newPassword;


    public ChangePasswordCredential(String username, String password, String newPassword) {
        this.userName = username;
        this.password = password;
        this.newPassword = newPassword;
    }

    public String getUsername() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getNewPassword() {
        return newPassword;
    }
}
