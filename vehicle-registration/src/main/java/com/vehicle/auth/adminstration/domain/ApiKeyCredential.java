package com.vehicle.auth.adminstration.domain;

public class ApiKeyCredential {
    private String username;
    private String password;
    private String apiKey;

    public ApiKeyCredential(String username, String password, String apiKey) {
        this.username = username;
        this.password = password;
        this.apiKey = apiKey;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getApiKey() {
        return apiKey;
    }
}
