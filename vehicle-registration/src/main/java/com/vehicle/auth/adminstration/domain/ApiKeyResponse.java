package com.vehicle.auth.adminstration.domain;

public class ApiKeyResponse {

    private String message;
    private String newApiKey;
    private int status;
    private String title;
    private String detail;

    public ApiKeyResponse(String message, String newApiKey, int status, String title, String detail) {
        this.message = message;
        this.newApiKey = newApiKey;
        this.status = status;
        this.title = title;
        this.detail = detail;
    }

    public ApiKeyResponse() {

    }

    public String getMessage() {
        return message;
    }

    public String getNewApiKey() {
        return newApiKey;
    }

    public int getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }

    @Override
    public String toString() {
        return "ApiKeyResponse{" +
                "message='" + message + '\'' +
                ", newApiKey='" + newApiKey + '\'' +
                ", status='" + status + '\'' +
                ", title='" + title + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
