package com.vehicle.auth.adminstration.repository;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.vehicle.auth.adminstration.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import static org.springframework.http.HttpHeaders.*;


@Repository
public class WebClientAuthenticationRepository implements AuthenticationRepository {

    @Value("${appplication.authentication.api.url}")
    private String baseUrl;


    private final HttpClient client;

    @Autowired
    public WebClientAuthenticationRepository() {
        client = HttpClient.newHttpClient();
    }


    @Override
    public Response<JWTToken> getToken(UserCredential userCredential) throws IOException, InterruptedException {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl+"/authenticate"))
                .header(CONTENT_TYPE, "application/json")
                .header(ACCEPT_CHARSET,"UTF-8")
                .POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(userCredential)))
                .build();

        HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
        JsonElement jsonElement;
        JWTToken jwtToken;
        if (httpResponse.statusCode() != 200) {
            jsonElement = JsonParser.parseString(httpResponse.body()).getAsJsonArray().get(0);
            jwtToken = new Gson().fromJson(jsonElement, JWTToken.class);
        } else {
            jsonElement = JsonParser.parseString(httpResponse.body()).getAsJsonObject();
            String token = jsonElement.getAsJsonObject().get("id-token").getAsString();
            jwtToken = new JWTToken(token,httpResponse.statusCode(),"","");
        }

        Response<JWTToken> response = new Response<JWTToken>("",
                HttpStatus.valueOf(httpResponse.statusCode()),
                jwtToken);
        return response;
    }


    @Override
    public <T> Response<Password> changeOrRecoverPassword(T t, Class<T> tClass) throws IOException, InterruptedException {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl+"/password"))
                .header(CONTENT_TYPE, "application/json")
                .header(ACCEPT_CHARSET,"UTF-8")
                .POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(t)))
                .build();

        HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());
        Password password;
        if(httpResponse.statusCode() == 200){
            password = new Password();
        }else {
           JsonElement jsonElement = JsonParser.parseString(httpResponse.body()).getAsJsonArray().get(0);
           password = new Gson().fromJson(jsonElement.toString(), Password.class);

        }

        Response<Password> response = new Response<>("Password successfully changed or recovered",
                HttpStatus.valueOf(httpResponse.statusCode()),
                password);
        return response;
    }

    @Override
    public Response<NewPasswordResponse> getNewPassword(NewPassword newPassword) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl+"/new-password"))
                .header(CONTENT_TYPE, "application/json")
                .header(ACCEPT_CHARSET,"UTF-8")
                .POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(newPassword)))
                .build();

        HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());

        JsonElement jsonElement = getJsonElement(httpResponse);

        NewPasswordResponse passwordResponse = new Gson().fromJson(jsonElement.toString(),NewPasswordResponse.class);

        Response<NewPasswordResponse> response = new Response(httpResponse.body(),
                HttpStatus.valueOf(httpResponse.statusCode()),
                passwordResponse);
        return response;
    }

    private JsonElement getJsonElement(HttpResponse<String> httpResponse) {
        JsonElement jsonElement;
        if (httpResponse.statusCode() != 200) {
            jsonElement = JsonParser.parseString(httpResponse.body()).getAsJsonArray().get(0);
        } else {
            jsonElement = JsonParser.parseString(httpResponse.body()).getAsJsonObject();
        }
        return jsonElement;
    }

    @Override
    public Response<ApiKeyResponse> getApiKeys(String apiKey,JWTToken jwtToken) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(baseUrl+"/new-api-key"))
                .header(CONTENT_TYPE, "application/json")
                .header(ACCEPT_CHARSET,"UTF-8")
                .header("x-api-key", apiKey)
                .header(AUTHORIZATION, jwtToken.getIdToken())
                .POST(HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> httpResponse = client.send(request, HttpResponse.BodyHandlers.ofString());

        JsonElement jsonElement = getJsonElement(httpResponse);

        ApiKeyResponse apiKeyResponse = new Gson().fromJson(jsonElement,ApiKeyResponse.class);

        Response<ApiKeyResponse> response = new Response<>("",
                HttpStatus.valueOf(httpResponse.statusCode()),
                apiKeyResponse);
        return response;
    }
}
