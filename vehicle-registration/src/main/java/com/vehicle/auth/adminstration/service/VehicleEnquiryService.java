package com.vehicle.auth.adminstration.service;

import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.domain.VehicleEnquiryRequest;
import com.vehicle.auth.adminstration.entity.RegistrationDetailsDto;
import com.vehicle.auth.adminstration.repository.VehicleEnquiryRepository;
import com.vehicle.auth.adminstration.repository.VehicleRegistrationRepository;

public class VehicleEnquiryService {

    private VehicleEnquiryRepository repository;
    private VehicleRegistrationRepository registrationRepository;
    public VehicleEnquiryService(VehicleEnquiryRepository vehicleEnquiryRepository, VehicleRegistrationRepository registrationRepository) {
            repository = vehicleEnquiryRepository;
            this.registrationRepository = registrationRepository;
    }

    public Response<RegistrationDetailsDto> getDetails(VehicleEnquiryRequest vehicleEnquiryRequest) {

        Response<RegistrationDetailsDto> response = repository.getDetails(vehicleEnquiryRequest);
        if(response.getStatus().value() == 200){
            RegistrationDetailsDto registrationDetails = response.getType();
            saveDetails(registrationDetails);
        }
        return response;
    }

    public void saveDetails(RegistrationDetailsDto registrationDetails){

        registrationRepository.save(registrationDetails);
    }
}
