package com.vehicle.auth.adminstration.repository;

import com.vehicle.auth.adminstration.domain.Response;
import com.vehicle.auth.adminstration.domain.VehicleEnquiryRequest;
import com.vehicle.auth.adminstration.entity.RegistrationDetailsDto;

public interface VehicleEnquiryRepository {
    Response<RegistrationDetailsDto> getDetails(VehicleEnquiryRequest vehicleEnquiryRequest);
}
