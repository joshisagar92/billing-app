package com.eyedyll.crime.controller;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.eyedyll.EyedyllApplicationTests;
import com.eyedyll.crime.entity.Crime;
import com.eyedyll.crime.stubs.CrimeStub;
import com.eyedyll.crime.stubs.StubFactory;
public class CrimeAcceptanceTest extends EyedyllApplicationTests {

	private CrimeStub crimeStub;
	
	@Override
	@Before
	public void setUp() {
		super.setUp();
		crimeStub = StubFactory.getCrimeStub();
	}
	
	
	@Test
	public void find_location_by_location_id() throws Exception {
		String date= "2017-02";
		String locationId="884227";
		String uri = "/api/crimes/"+date+"/"+locationId;
		String expectedCrimesJSON = this.readJSON("classpath:crimes.json");
		Crime[] expectedCrimes = this.mapFromJson(expectedCrimesJSON, Crime[].class);
		crimeStub.getCrimes(expectedCrimesJSON, date, locationId);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		Crime[] crimes = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Crime[].class);
		assertArrayEquals(expectedCrimes, crimes);
	}

}
