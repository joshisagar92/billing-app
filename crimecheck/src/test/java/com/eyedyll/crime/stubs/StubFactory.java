package com.eyedyll.crime.stubs;

public class StubFactory {

	private static CrimeStub crimeStub;

	public static CrimeStub getCrimeStub() {
		return crimeStub;
	}

	public static void setCrimeStub(CrimeStub crimeStub) {
		StubFactory.crimeStub = crimeStub;
	}
	
	
}
