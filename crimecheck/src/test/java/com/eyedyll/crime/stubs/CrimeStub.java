package com.eyedyll.crime.stubs;

import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

import org.springframework.http.HttpStatus;
public class CrimeStub {
	 public  WireMockServer wireMockServer;

	public CrimeStub(WireMockServer wireMockServer) {
		super();
		this.wireMockServer = wireMockServer;
	}
	 
	 public void getCrimes(String body,String date,String locationId) {
		 wireMockServer.stubFor(
	                get("/thirdparty-access/v1/")
	                .willReturn(aResponse().withBody(body).withStatus(HttpStatus.OK.value())));
	 }
}
