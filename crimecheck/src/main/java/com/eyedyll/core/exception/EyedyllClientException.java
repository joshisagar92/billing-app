package com.eyedyll.core.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class EyedyllClientException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	HttpStatus httpStatus;
	
	String message;

	public EyedyllClientException(HttpStatus httpStatus, String message) {
		super();
		this.httpStatus = httpStatus;
		this.message = message;
	}
	
	
	
}
