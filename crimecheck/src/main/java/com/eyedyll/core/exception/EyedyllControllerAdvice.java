package com.eyedyll.core.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.eyedyll.core.exception.model.ApiError;

@ControllerAdvice
public class EyedyllControllerAdvice {

	@ExceptionHandler(EyedyllClientException.class)
	   protected ResponseEntity<Object> handleRestError(EyedyllClientException ex) {
	       ApiError apiError = new ApiError(ex.getHttpStatus(),ex);
	       apiError.setMessage(ex.getMessage());
	       return buildResponseEntity(apiError);
	   }
	
	
	private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
	       return new ResponseEntity<>(apiError, apiError.getStatus());
	   }
}
