package com.eyedyll.core.exception.model;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class ApiError {
	   private HttpStatus status;
	   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	   private LocalDateTime timestamp;
	   private String message;
	   
	   public ApiError(HttpStatus status, Throwable ex) {
		   this.status = status;
		   this.message = ex.getLocalizedMessage();
	       timestamp = LocalDateTime.now();
	   }
	   
	   
}
