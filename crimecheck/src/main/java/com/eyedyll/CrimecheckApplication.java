package com.eyedyll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrimecheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrimecheckApplication.class, args);
    }

}
