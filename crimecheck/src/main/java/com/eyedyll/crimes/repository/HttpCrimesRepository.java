package com.eyedyll.crimes.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.eyedyll.core.exception.RestErrorHandler;
import com.eyedyll.crime.entity.Crime;
import com.google.common.collect.ImmutableMap;


@Repository
public class HttpCrimesRepository implements CrimesRepository {
	
	@Value("${appplication.eyedyll.crime.api.url}")
	private String url;
	
	private RestTemplate restTemplate;
	
	@Autowired
	public HttpCrimesRepository(RestTemplateBuilder restTemplateBuilder) {
		restTemplate = restTemplateBuilder.errorHandler(new RestErrorHandler())
				.build();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
		messageConverters.add(converter);
		this.restTemplate.setMessageConverters(messageConverters);
	}
	
	@Override
	public List<Crime> getCrimeLocationDetailsById(String date, long locationId) {
		Map<String, Object> uriParams = ImmutableMap.of("date", date, "location", locationId);
		return get(url,uriParams);
	}

	@Override
	public List<Crime> getCrimeLocationDetailsByLongitudeAndLatitude(String date, String latitude, String longitude) {
		Map<String, Object> uriParams = ImmutableMap.of("date", date, "lat", latitude,"lng",longitude);
		return get(url,uriParams);		
	}
	
	private List<Crime> get(String url,Map<String,Object> uriParams){
		return restTemplate.exchange(url, HttpMethod.GET,null,
				new ParameterizedTypeReference<List<Crime>>() {}).getBody();
	}

}
