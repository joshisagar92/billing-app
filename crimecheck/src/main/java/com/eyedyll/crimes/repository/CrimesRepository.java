package com.eyedyll.crimes.repository;

import java.util.List;

import com.eyedyll.crime.entity.Crime;

public interface CrimesRepository {

	public List<Crime> getCrimeLocationDetailsById(String date, long locationId);
	
	public List<Crime> getCrimeLocationDetailsByLongitudeAndLatitude(String date, String latitude, String longitude);
}
