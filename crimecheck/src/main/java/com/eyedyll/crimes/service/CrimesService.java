package com.eyedyll.crimes.service;

import java.util.List;

import com.eyedyll.crime.entity.Crime;
import com.eyedyll.crimes.repository.CrimesRepository;

public class CrimesService {

	private CrimesRepository crimesRepository;
	
	public CrimesService(CrimesRepository crimesRepository) {
		super();
		this.crimesRepository = crimesRepository;
	}

	public List<Crime> getCrimeLocationDetailsById(String date, long locationId) {
		return crimesRepository.getCrimeLocationDetailsById(date, locationId);
	}

	public List<Crime> getCrimeLocationDetailsByLongitudeAndLatitude(String date, String latitude, String longitude) {
		return crimesRepository.getCrimeLocationDetailsByLongitudeAndLatitude(date, latitude, longitude);
	}
	
	
	
}
