package com.eyedyll.crime.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eyedyll.crime.entity.Crime;
import com.eyedyll.crimes.repository.CrimesRepository;
import com.eyedyll.crimes.service.CrimesService;

@RestController
@RequestMapping("api/crimes/")
public class CrimeController {

	private CrimesService crimesService;
	
	@Autowired
	public CrimeController(CrimesRepository crimesRepository) {
		crimesService = new CrimesService(crimesRepository);
	}
	
	@GetMapping("{date}/{locationId}")
	public ResponseEntity<List<Crime>> findByLocationId(@PathVariable("date") String date, @PathVariable("locationId") long locationId) {
		return new ResponseEntity<List<Crime>>(crimesService.getCrimeLocationDetailsById(date, locationId), HttpStatus.OK);
	}
	
	@GetMapping("{date}/{latitude}/{longitude}")
	public ResponseEntity<List<Crime>>  findByLocationId(@PathVariable("date") String date, @PathVariable("latitude") String latitude, @PathVariable("longitude") String longitude) {
		return new ResponseEntity<List<Crime>>(crimesService.getCrimeLocationDetailsByLongitudeAndLatitude(date, latitude,longitude), HttpStatus.OK);
	}
	
}
