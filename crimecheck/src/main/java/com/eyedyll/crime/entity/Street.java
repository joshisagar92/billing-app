package com.eyedyll.crime.entity;

import lombok.Data;

@Data
public class Street {
	
	private long id;
	
	private String name;

}
