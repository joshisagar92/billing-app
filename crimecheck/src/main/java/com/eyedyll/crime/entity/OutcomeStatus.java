package com.eyedyll.crime.entity;

import lombok.Data;

@Data
public class OutcomeStatus {
	private String category;
	private String date;
	
}
