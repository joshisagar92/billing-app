package com.eyedyll.crime.entity;

import lombok.Data;

@Data
public class Location {

	private String latitude;
	private String longitude;
	
	private Street street;
	
	
	
}
